#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

cp ./resybotd /etc/init.d/resybotd
cp ./resybot /bin/resybot
systemctl daemon-reload
service resybotd restart
systemctl enable resybotd
