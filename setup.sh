#!/bin/bash

sudo apt-get update
sudo apt-get install python3-pip
sudo apt-get install chromium-browser --yes
sudo apt-get install chromium-chromedriver --yes
sudo pip3 install selenium
sudo pip3 install schedule

