#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

service resybotd stop
rm /etc/init.d/resybotd
rm /bin/resybot
